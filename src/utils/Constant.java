package utils;

public class Constant {
	public static final int DEFAULT_TIMEOUT = 30;
	
	public static final String EXAM_LOGIN_URL = "https://training.huy.kim/exam/login.html";
	public static final String EXAM_EDIT_PROFILE_URL = "https://training.huy.kim/exam/profile.html";
	
	
}
