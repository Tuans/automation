package page.object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogoutPageExam extends BasePage{

	By profileMenuLocator;
	By logoutButtonLocator;
	By lblLoginLocator;
	
	
	public By getLblLoginLocator() {
		return By.xpath(".//h2[contains(.,'Please sign in')]");
	}


	public By getProfileMenuLocator() {
		return By.xpath(".//a[@class='dropdown-toggle']");
	}


	public By getLogoutButtonLocator() {
		return By.xpath(".//a[@href='login.html']");
	}
	
	public WebElement getLblLogin() {
		WebElement lblLogin = null;
		try {
			lblLogin = driverWait.until(ExpectedConditions
					.presenceOfElementLocated(getLblLoginLocator()));
		} catch (Exception e) {
			System.out.println("Not found element in timeout session"
					+ e.getMessage());
		}
		return lblLogin;
	}

	public void gotoDashbboard(){
		LoginPageExam loginPageExam =new LoginPageExam(driver, driverWait);
		loginPageExam.goToLoginPage();
		loginPageExam.doLogin("tuanna34@fsoft.com.vn", "123");
		driverWait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath(".//h1[contains(.,'Dashboard')]")));
	}

	public String doLogout(){
		myClick(getProfileMenuLocator());
		myClick(getLogoutButtonLocator());
		String currentUrl = driver.getCurrentUrl();
		return currentUrl;
	}
	public LogoutPageExam(WebDriver driver, WebDriverWait driverWait) {
		super(driver, driverWait);
		// TODO Auto-generated constructor stub
	}

}
