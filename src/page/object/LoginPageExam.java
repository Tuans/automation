package page.object;

import static utils.Constant.EXAM_LOGIN_URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPageExam extends BasePage {

	 By txtEmailAddressLocator;
	 By txtPasswordLocator;
	 By btnLoginLocator;
	 By lblMessageErrorLocator;

	public By getTxtEmailAddressLocator() {
		return By.xpath(".//input[@id='inputEmail']");
	}

	public By getTxtPasswordLocator() {
		return By.xpath(".//input[@id='inputPassword']");
	}

	public By getBtnLoginLocator() {
		return By.xpath(".//button[contains(.,'Sign in')]");
	}

	public By getLblMessageErrorLocator() {
		return By
				.xpath(".//div[@id='messages']/div[contains(.,'is incorrect. Please use fsoft account (xxx@fsoft.com.vn)')]");
	}

	public LoginPageExam(WebDriver driver, WebDriverWait driverWait) {
		super(driver, driverWait);
		// TODO Auto-generated constructor stub
	}

	public WebElement getLblMessageError() {
		WebElement lblMessageError = null;
		try {
			lblMessageError = driverWait.until(ExpectedConditions
					.presenceOfElementLocated(getLblMessageErrorLocator()));
		} catch (Exception e) {
			System.out.println("Not found element in timeout session"
					+ e.getMessage());
		}
		return lblMessageError;
	}

	// ---ACTIONS----
		/**
		 * Go to Login Page
		 */
		public void goToLoginPage() {
			driver.get(EXAM_LOGIN_URL);
			driver.manage().window().maximize();
			driverWait.until(ExpectedConditions.presenceOfElementLocated(getTxtEmailAddressLocator()));
		}
	
		/**
		 * Do Login
		 */
		public void doLogin(String email, String password) {
			mySendKey(getTxtEmailAddressLocator(), email);
			mySendKey(getTxtPasswordLocator(), password);

			driver.findElement(getBtnLoginLocator()).click();
		}
}
