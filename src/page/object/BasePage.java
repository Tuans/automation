package page.object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	WebDriver driver;
	WebDriverWait driverWait;

	public BasePage(WebDriver driver, WebDriverWait driverWait) {
		this.driver = driver;
		this.driverWait = driverWait;
	}

	public void mySendKey(By elementLocator, String message) {
		WebElement element = driverWait.until(ExpectedConditions
				.presenceOfElementLocated(elementLocator));
		element.sendKeys(message);
	}
	
	public void myClick(By elementLocator) {
		WebElement element = driverWait.until(ExpectedConditions
				.presenceOfElementLocated(elementLocator));
		
		element.click();
	}
}
