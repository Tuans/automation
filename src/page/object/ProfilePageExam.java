package page.object;

import static utils.Constant.EXAM_EDIT_PROFILE_URL;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfilePageExam extends BasePage {

	public ProfilePageExam(WebDriver driver, WebDriverWait driverWait) {
		super(driver, driverWait);
		// TODO Auto-generated constructor stub
	}

	By txtFullNameLocator;
	By txtDateOfBirthLocator;
	By rdGenderLocator;
	By txtSkillsLocator;
	By txtPhoneNumberLocator;
	By chbLanguagesKnowLocator;
	By txtAreaOverviewLocator;
	By btnSubmitLocator;
	By lblMessageErrorLocatorLocator;
	WebElement lblMessageError;

	public By getTxtFullNameLocator() {
		return By.xpath(".//input[@id='full-name']");
	}

	public By getTxtDateOfBirthLocator() {
		return By.xpath(".//input[@id='Date Of Birth']");
	}

	public By getRdGenderLocator(String gender) {
		return By.xpath(".//label[@class='radio-inline' and contains(.,'"
				+ gender + "')]");
	}

	public By getTxtSkillsLocator() {
		return By.xpath(".//input[@id='Skills']");
	}

	public By getTxtPhoneNumberLocator() {
		return By.xpath(".//input[@id='Phone number ']");
	}

	public By getChbLanguagesKnowLocator(String language) {
		return By.xpath(".//label[contains(.,'" + language + "')]//input");
	}

	public By getTxtAreaOverviewLocator() {
		return By.xpath(".//textarea[@id='Overview (max 200 words)']");
	}

	public By getBtnSubmitLocator() {
		return By.xpath(".//button[@type='submit']");
	}

	public By getLblMessageErrorLocator() {
		return By
				.xpath(".//div[@id='messages']/div[contains(.,'Please input full name')]");
	}

	public WebElement getLblMessageError() {
		WebElement lblMessageError = null;
		try {
			lblMessageError = driverWait.until(ExpectedConditions
					.presenceOfElementLocated(getLblMessageErrorLocator()));
		} catch (Exception e) {
			System.out.println("Not found element in timeout session"
					+ e.getMessage());
		}
		return lblMessageError;
	}

	public String getAlertMessageSuccess() {
		driverWait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		String message = alert.getText();
		alert.accept();
		return message;
	}

	// ---ACTIONS----
	/**
	 * Go to edit profile Page
	 */
	public void goToEditprofilePage() {
		LoginPageExam loginPageExam = new LoginPageExam(driver, driverWait);
		loginPageExam.goToLoginPage();
		loginPageExam.doLogin("tuanna34@fsoft.com.vn", "123");
		driverWait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath(".//h1[contains(.,'Dashboard')]")));
		driver.get(EXAM_EDIT_PROFILE_URL);

		driverWait.until(ExpectedConditions
				.presenceOfElementLocated(getBtnSubmitLocator()));
	}

	/**
	 * Do edit profile
	 */
	public String doEditProfile(String fullName, String dateOfBirth,
			String gender, String skills, String phoneNumber,
			List<String> languagesKnow, String overView) {
		mySendKey(getTxtFullNameLocator(), fullName);
		mySendKey(getTxtDateOfBirthLocator(), dateOfBirth);
		myClick(getRdGenderLocator(gender));
		mySendKey(getTxtSkillsLocator(), skills);
		mySendKey(getTxtPhoneNumberLocator(), phoneNumber);

		if (languagesKnow != null)
			for (String language : languagesKnow) {
				myClick(getChbLanguagesKnowLocator(language));
			}
		mySendKey(getTxtAreaOverviewLocator(), overView);
		driver.findElement(getBtnSubmitLocator()).click();

		if (fullName != null && !fullName.isEmpty()) {
			driverWait.until(ExpectedConditions.alertIsPresent());
			try {
				Alert alert = driver.switchTo().alert();
				String message = alert.getText();
				alert.accept();
				return message;
			} catch (Exception e) {
				System.out.println("Done have any lert dialog");
			}
		}
		return "";
	}
}
