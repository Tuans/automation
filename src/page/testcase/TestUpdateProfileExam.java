package page.testcase;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import page.object.ProfilePageExam;
import utils.Constant;

import com.google.common.collect.ImmutableList;

public class TestUpdateProfileExam {
	WebDriver driver;
	WebDriverWait driverWait ;
	
	@Test(dataProvider = "dataEditProfile")
	public void testLogin(String fullName, String dateOfBirth, String gender, String skills, String phoneNumber, List<String> languagesKnow, String overView) {
		ProfilePageExam profilePageExam = new ProfilePageExam(driver, driverWait);
		
		profilePageExam.goToEditprofilePage();
		String message = profilePageExam.doEditProfile(fullName, dateOfBirth, gender, skills, phoneNumber, languagesKnow, overView);
		if(fullName!=null && fullName.isEmpty()){
			System.out.println("fullName is empty");
			assertNotNull(profilePageExam.getLblMessageError());
		}else{
			System.out.println("fullName: " + fullName);
			assertEquals(message, "Thanks " + fullName + ". Your details submitted successfully.");
		}
	}

	@BeforeMethod
	public void beforeMethod() {
		driver = new FirefoxDriver();
		driverWait = new WebDriverWait(driver, Constant.DEFAULT_TIMEOUT);
		
	}

	@AfterMethod
	public void afterMethod() {
		if(driver!=null){
			driver.quit();
		}
	}

	@DataProvider
	public Object[][] dataEditProfile() {
		return new Object[][] { new Object[] { "Nguyen Anh Tuan",// fullName
				"05/04/1991",// dateOfBirth
				"Male",// gender
				"Gai, Ruoi, Bia, Co bac",// skills
				"01656098166",// phoneNumber
				ImmutableList.of("Japanese", "Vietnamese"),// languagesKnow
				"Handsome and rich" }, // overView
				new Object[] { "",// fullName
						"05/04/1991",// dateOfBirth
						"Male",// gender
						"Gai, Ruoi, Bia, Co bac",// skills
						"01656098166",// phoneNumber
						ImmutableList.of("Japanese", "Vietnamese"),// languagesKnow
						"Handsome and rich" } // overView
		};
	}
}
