package page.testcase;

import static org.testng.Assert.assertNotNull;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import page.object.LogoutPageExam;
import utils.Constant;

public class TestLogoutExam {
	WebDriver driver;
	WebDriverWait driverWait ;
	
	@Test
	public void testLogout() {
		LogoutPageExam logoutPageExam = new LogoutPageExam(driver, driverWait);
		logoutPageExam.gotoDashbboard();
		logoutPageExam.doLogout();
		assertNotNull(logoutPageExam.getLblLogin());
	}

	@BeforeMethod
	public void beforeMethod() {
		driver = new FirefoxDriver();
		driverWait = new WebDriverWait(driver, Constant.DEFAULT_TIMEOUT);
		
	}

	@AfterMethod
	public void afterMethod() {
		if(driver!=null){
			driver.quit();
		}
	}
}
