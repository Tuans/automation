package page.testcase;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

import java.sql.Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import page.object.LoginPageExam;
import utils.Constant;

public class TestLoginExam {
	WebDriver driver;
	WebDriverWait driverWait ;
	
	@Test(dataProvider = "dataLogin")
	public void testLogin(String userName, String password) {
		LoginPageExam loginPageExam = new LoginPageExam(driver, driverWait);
		loginPageExam.goToLoginPage();
		loginPageExam.doLogin(userName, password);
		if(userName!=null && userName.contains("@fsoft.com.vn")){
			assertNull(loginPageExam.getLblMessageError());
		}else{
			assertNotNull(loginPageExam.getLblMessageError());
		}
	}

	@BeforeMethod
	public void beforeMethod() {
		driver = new FirefoxDriver();
		driverWait = new WebDriverWait(driver, Constant.DEFAULT_TIMEOUT);
		
	}

	@AfterMethod
	public void afterMethod() {
		if(driver!=null){
			driver.quit();
		}
	}

	@DataProvider
	public Object[][] dataLogin() {
		return new Object[][] { 
				new Object[] { "tuanna34@fsoft.com.vn", "pass001" },
				new Object[] { "123@56.8", "123" }
				};
	}
	
	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
	}
}
